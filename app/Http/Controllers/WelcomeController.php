<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class WelcomeController extends Controller
{
    public function index(){
       return view('frontendPage.index');
    }
    public function catagory(){
         return view('frontendPage.catagory.catagoryContent');
    }
    public function contact(){
        return view('frontendPage.contact.contactPage');
    }
    public function productView(){
        return view('frontendPage.product.productDetails');
    }
}
