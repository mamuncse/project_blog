<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/','WelcomeController@index');
Route::get('/catagory','WelcomeController@catagory');
Route::get('/contact','WelcomeController@contact');
Route::get('/product-view','WelcomeController@productView');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
