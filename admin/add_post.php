<?php include'inc/header.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>admin post</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class="page_wrap">
	<div class="post">
	<h2>Add New Post</h2>
	<?php
	if ($_SERVER['REQUEST_METHOD']=='POST') {
		$title=mysqli_real_escape_string($db->link,$_POST['title']);
		$cat=mysqli_real_escape_string($db->link,$_POST['cat']);
		$body=mysqli_real_escape_string($db->link,$_POST['body']);
		$tags=mysqli_real_escape_string($db->link,$_POST['tags']);
		$author=mysqli_real_escape_string($db->link,$_POST['author']);
		 $permited  = array('jpg', 'jpeg', 'png', 'gif');
         $file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $file_temp = $_FILES['image']['tmp_name'];
        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
        $uploaded_image = "upload/".$unique_image;
        if ($title==" "|| $cat==" "|| $body==" "|| $tags==" "|| $author==" "|| $file_name==" ") {
        	echo "<span class='error'>field must not be empty !!</span>";
        }elseif ($file_size>1048567) {
        echo "<span class='error'>Image size should be less then 1MB !!</span>";
         }
        elseif (in_array($file_ext, $permited)==false) {
         echo "<span class='error'>Your can upload only".implode(', ',$permited)." !!</span>";
	}else{
		move_uploaded_file($file_temp, $uploaded_image);
        $query = "INSERT INTO tbl_post(cat,title,body,image,author,tags) VALUES('$cat','$title','$body','$uploaded_image','$author','$tags')";
        $inserted_rows = $db->insert($query);
        if ($inserted_rows) {
         echo "<span class='success'>data Inserted Successfully.
         </span>";
        }else {
         echo "<span class='error'>data Not Inserted !</span>";
        }
    }
}

	?>
	<form action="add_post.php" method="POST" enctype="multipart/form-data">
	<table>
	<tr>
		<td><label>Title</label></td>
		<td><input type="text" name="title"></td>
		</tr>
		<tr>
		<td><label>Catagory</label></td>
		<td><select id="select" name="cat">
			<option>select catagory</option>
			<?php 
				$query="select * from tbl_catagory";
				$catagory=$db->select($query);
				if ($catagory) {
					while ($result=$catagory->fetch_assoc()) {
			?>
			<option value="<?php echo $result['id'];?>"><?php echo $result['name'];?></option>
			<?php } } ?>
		</select>
		</td>
		</tr>
		<tr>
			<td><label>Upload Image</label></td>
			<td><input type="file" name="image"></td>
		</tr>
		<tr>
			<td style="vertical-align:top; padding-top:9px; "><label>content</label></td>
			<td><textarea class="tinymce" name="body"></textarea></td>
		</tr>
		<tr>
		<td>
		<label>tags</label>	
		</td>
		<td><input type="text" name="tags"></td>
		</tr>
		<tr>
			<td>
				<label>Author</label>
			</td>
			<td><input type="text" name="author"></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" name="submit" value="POST"></td>
		</tr>
		</table>
	</form>
	</div>
</div>
</body>
</html>