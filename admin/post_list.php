<?php include'inc/header.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>post list...</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class="page_wrap">
<div class="page">
<form action="" method="POST">
<h1>POST LIST</h1>
	<table border="1">
		<tr>
			<th width="5%">no</th>
			<th width="15%">Title</th>
			<th width="20%">Description</th>
			<th width="10%">Catagory</th>
			<th width="10%">Image</th>
			<th width="10%">author</th>
			<th width="10%">tags</th>
			<th width="10%">date</th>
			<th width="10%">Action</th>
		</tr>
		<?php $query="SELECT tbl_post.*,tbl_catagory.name FROM tbl_post INNER JOIN tbl_catagory ON tbl_post.cat=tbl_catagory.id 
		ORDER BY tbl_post.title DESC";
		$post=$db->select($query);
		if ($post) {
			$i=0;
			while ($result=$post->fetch_assoc()) {
				$i++;
		?>
		<tr>
			<td><?php echo $i; ?></td>
			<td><a href="edit_post?edit_id=<?php echo $result['id'];?>"><?php echo $result['title'];?></a></td>
			<td><?php echo $fm->textShort($result['body'],100);?></td>
			<td><?php echo $result['name'];?></td>
			<td><img src="<?php echo $result['image']; ?>" height="40px" width="60px" ></td>
			<td><?php echo $result['author'];?></td>
			<td><?php echo $result['tags'];?></td>
			<td><?php echo$fm->formatDate($result['date']);?></td>
			<td><a href="edit_post.php?edit_id=<?php echo $result['id'];?>">Edit</a>||<a onclick="return confirm('Are you sure to Delete!!');" href="delete_post.php?delete_id=<?php echo $result['id'];?>">Delete</a></td>
		</tr>
		<?php } ?>
		<?php }else { ?>
			<p>no data post list</p>
			<?php } ?>
	</table>
</form>
</td>
</div>
</body>
</html>