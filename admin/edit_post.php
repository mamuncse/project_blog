<?php include'inc/header.php'; ?>
<!DOCTYPE html>
<html>
<head>
	<title>admin post</title>
	<link rel="stylesheet" type="text/css" href="style.css">
</head>
<body>
<div class="page_wrap">
	<div class="post">
	<?php
if ($_GET['edit_id']==' ' || $_GET['edit_id']==NULL) {
 	echo "<script>window.location='post_list.php';</script>";
 } else{
 	$id=$_GET['edit_id'];
 }
?>
	<h2>Update Post</h2>
	<?php
	if ($_SERVER['REQUEST_METHOD']=='POST') {
		$title=mysqli_real_escape_string($db->link,$_POST['title']);
		$cat=mysqli_real_escape_string($db->link,$_POST['cat']);
		$body=mysqli_real_escape_string($db->link,$_POST['body']);
		$tags=mysqli_real_escape_string($db->link,$_POST['tags']);
		$author=mysqli_real_escape_string($db->link,$_POST['author']);
		 $permited  = array('jpg', 'jpeg', 'png', 'gif');
        $file_name = $_FILES['image']['name'];
        $file_size = $_FILES['image']['size'];
        $file_temp = $_FILES['image']['tmp_name'];
        $div = explode('.', $file_name);
        $file_ext = strtolower(end($div));
        $unique_image = substr(md5(time()), 0, 10).'.'.$file_ext;
        $uploaded_image = "upload/".$unique_image;
        if ($title==" "|| $cat==" "|| $body==" "|| $tags==" "|| $author==" ") {
        	echo "<span class='error'>field must not be empty !!</span>";
        }else{
        if (!empty($file_name)) {
  
        if ($file_size>1048567) {
        echo "<span class='error'>Image size should be less then 1MB !!</span>";
         }
        elseif (in_array($file_ext, $permited)==false) {
         echo "<span class='error'>Your can upload only".implode(', ',$permited)." !!</span>";
	}else{
		move_uploaded_file($file_temp, $uploaded_image);
        $query="UPDATE tbl_post
    	SET 
    	cat='$cat',
    	title='$title',
    	body='$body',
    	image='$uploaded_image',
    	author='$author',
    	tags='$tags'
    	WHERE id='$id'";
        $update_rows = $db->update($query);
        if ($update_rows) {
         echo "<span class='success'>data updated Successfully.
         </span>";
        }else { 
         echo "<span class='error'>data Not updated !</span>";
        }
    }
}else{
        	 $query="UPDATE tbl_post
    	SET 
    	cat='$cat',
    	title='$title',
    	body='$body',
    	author='$author',
    	tags='$tags'
    	WHERE id='$id'";
        $update_rows = $db->update($query);
        if ($update_rows) {
         echo "<span class='success'>data updated Successfully.
         </span>";
        }else {
         echo "<span class='error'>data Not updated !</span>";

        }
    }
    }
}

	?>
	<?php 
	$query="SELECT * FROM tbl_post WHERE id='$id' ORDER BY id DESC";
	$edit_post=$db->select($query);
	if ($edit_post) {
		while ($post_result=$edit_post->fetch_assoc()) {
	?>
	<form action="" method="POST"  enctype="multipart/form-data">
	<table>
	<tr>
		<td><label>Title</label></td>
		<td><input type="text" name="title" value="<?php echo $post_result['title']; ?>"></td>
		</tr>
		<tr>
		<td><label>Catagory</label></td>
		<td><select id="select" name="cat">
			<option>select catagory</option>
			<?php 
				$query="select * from tbl_catagory";
				$catagory=$db->select($query);
				if ($catagory) {
					while ($result=$catagory->fetch_assoc()) {
			?>
			<option 
				<?php 
			if ($post_result['cat']==$result['id']) { ?>
			selected="selected"
				<?php } ?>
			value="<?php echo $result['id'];?>"><?php echo $result['name'];?></option>
			<?php } } ?>
		</select>
		</td>
		</tr>
		<tr>
			<td><label>Upload Image</label></td>
			<td>
			<img src="<?php echo $post_result['image']; ?>" height="100px"; width="160px"/><br/>
			<input type="file" name="image"></td>
		</tr>
		<tr>
			<td style="vertical-align:top; padding-top:9px; "><label>content</label></td>
			<td><textarea class="tinymce" name="body">
				 <?php echo $post_result['body']; ?>
			</textarea></td>
		</tr>
		<tr>
		<td>
		<label>tags</label>	
		</td>
		<td><input type="text" name="tags"  value="<?php echo $post_result['tags']; ?>"></td>
		</tr>
		<tr>
			<td>
				<label>Author</label>
			</td>
			<td><input type="text" name="author"  value="<?php echo $post_result['author']; ?>"></td>
		</tr>
		<tr>
			<td></td>
			<td><input type="submit" name="submit" value="POST"></td>
		</tr>
		</table>
	</form>
	<?php } } ?>
	</div>
</div>
</body>
</html>